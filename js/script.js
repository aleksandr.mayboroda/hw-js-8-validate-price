let resultDiv = document.querySelector('.result')

start(resultDiv)
function start(mainDiv) {
    mainDiv.insertAdjacentHTML('afterbegin',`
    <div class="form-element">
        <label>Price, $
            <input type="text" name="price" class="input-text">
        </label>
    </div>
    `)
    
    let priceInput = document.querySelector('input[name="price"]'),
        inputBlock = document.querySelector('.form-element')

    //общий обработчик на клик в блоке
    inputBlock.addEventListener('click',(event) => {

        // удаление кнопки с ценой
        if(event.target.classList.contains('btn-close'))
        {
            deletePriceBtn(event.currentTarget)
            priceInput.value = ''
            //удаляем зеленый цвет текста
            event.currentTarget.querySelector('input').classList.remove('text-green')
        }
    })
    
    // изменение на инпуте
    priceInput.addEventListener('change', function(event) {
        //если это наш инпут
        if(event.target.name === 'price')
        {
            validateInput(event.target,'Please enter correct price')
        }
    })
}

// валидация поля
function validateInput(input,errorText) {

    if(isNaN(input.value) || parseFloat(input.value) <= 0 || isNaN(parseFloat(input.value)))
    {
        // удаление
        deletePriceBtn(input.closest('div'))
        input.classList.remove('text-green')
        input.classList.add('input-error')
        // выводим ошибку
        toggleError(input,errorText)
        return false
    }

    //создание 
    createPriceBtn(input)
    // цвет шрифта
    input.classList.add('text-green')
    // добавляем класс ошибки на блок
    input.classList.remove('input-error')
    // выводим ошибку
    toggleError(input)
    return true
}

function createPriceBtn(input) {
    let priceButton, buttonClose
   
    //удаляем кнопку, если есть
    deletePriceBtn(input.closest('div'))

    input.value = parseFloat(input.value)

    priceButton = document.createElement('span')
    buttonClose = document.createElement('span')
    priceButton.className = 'btn-price'
    priceButton.innerText = `Текущая цена: ${input.value}`
    buttonClose.innerText = 'x'
    buttonClose.className = 'btn-close'

    priceButton.insertAdjacentElement('beforeend',buttonClose)
    input.parentElement.before(priceButton)  
}

//удаляем кнопку
function deletePriceBtn(parentDiv) {
    // let existedPriceBtn = document.querySelector('.btn-price')
    let existedPriceBtn = parentDiv.querySelector('.btn-price')
    if(existedPriceBtn)
    {
        existedPriceBtn.remove()
    }
}

// отображение ошибки
function toggleError(elem,errorText = '') {
    let errorBlock = elem.closest('div').querySelector('.form-error'),
        newErrBlock

    if(errorBlock)
    {
        errorBlock.remove()
    }
   
    if(errorText)
    {
        newErrBlock = document.createElement('p')
        newErrBlock.className = 'form-error'
        newErrBlock.innerText = errorText
        elem.closest('div').insertAdjacentElement('beforeEnd',newErrBlock)
    }

}